@if(count($relatedProduct) > 0)
    <section class="site-content">
        <div class="container">
            <div class="products-area">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-spacial p-0">
                        <div class="paanel custom-panel-style">
                            <div class="panel-heading ">
                                <h3 class="section-heading">Similar products</h3>
                            </div>
                            <div class="panel-body plr-0">
                                <div id="special" role="tabpanel" aria-labelledby="special-tab">
                                    <div class="owl-carousel owl-loaded owl-drag">
                                        <div class="owl-stage-outer">
                                            <div class="owl-stage">
                                                @foreach($relatedProduct as $key=>$rProductSingle)
                                                    <div class="owl-item active">
                                                        <div class="product-grid mtb-15">
                                                            <div class="product-image">
                                                                <a href="{{url('/product/'.$rProductSingle->slug)}}" class="image">
                                                                    <img class="pic-1" src="{!! SM::sm_get_the_src($rProductSingle->image, 200,200) !!}">
                                                                </a>
                                                                {{--<span class="product-new-label">new</span>--}}
                                                                <span class="product-discount-label">{{SM::productDiscount($rProductSingle->id )}} %</span>
                                                                {{--<ul class="social">--}}
                                                                {{--<li><a href="#"><i class="fa fa-eye"></i></a></li>--}}
                                                                {{--<li><a href="#"><i class="fa fa-heart"></i></a></li>--}}
                                                                {{--</ul>--}}
                                                            </div>
                                                            <div class="product-content">
                                                                <a href="{{url('/product/'.$rProductSingle->slug)}}" class="title-color mb-1">{{$rProductSingle->title}}</a>
                                                                @if($rProductSingle->sale_price > 0)
                                                                    <div class="price"><span>{{ SM::currency_price_value($rProductSingle->regular_price) }} </span> {{ SM::currency_price_value($rProductSingle->sale_price) }} </div>
                                                                @else
                                                                    <div class="price"> {{ SM::currency_price_value($rProductSingle->regular_price) }} </div>
                                                                @endif
                                                                <div class="product-link inc_dec_qty">
                                                                    <a href="#" class=""> {{$rProductSingle->product_weight}} {{SM::product_wait_unit($rProductSingle->unit_id)->title}}</a>
                                                                    <?php  echo SM::addToCartButton($rProductSingle->id, $rProductSingle->regular_price, $rProductSingle->sale_price) ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif