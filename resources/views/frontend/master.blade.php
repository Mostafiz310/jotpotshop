<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
@include('frontend.common.meta')
@include('frontend.inc.css')
@include('frontend.common.additional_css')
@stack('style')
<!--------- stripe js ------>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script><style>
    .col-md-7.col-12.order-md-1.order-2 {
padding-top: 112px;
}
</style>
</head>
<body>
<div id="fb-root"></div>
@include('frontend.common.only_login_Modal')
@include('frontend.common.register_model')
@include('frontend.common.login_modal')
@include('frontend.inc.header')
<div class="container">
    <div class="row">
            @include('frontend.common.s_w_message')
            <div class="search-html">
                @yield('content')
            </div>
      </div>
</div>
@include('frontend.inc.footer_top')
@include('frontend.inc.footer')
<!-- all js scripts including custom js -->
<!-- scripts -->
@include('frontend.inc.js')
@include('frontend.common.additional_js')
@stack('script')


</body>
</html>


