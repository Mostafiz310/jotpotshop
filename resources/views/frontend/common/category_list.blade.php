<?php $is_featureds = SM::getIsFeaturedCategories(); ?>
@if(count($is_featureds)>0)
    <section class="site-content">
        <div class="container">
            <div class="products-area">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-spacial p-0">
                        <div class="paanel custom-panel-style">
                            <div class="panel-heading">
                                <h3 class="section-heading">Product Category</h3>
                            </div>
                            <div class="panel-body plr-0">
                                <div class="row">
                                    @foreach($is_featureds as $is_featured)
                                        <div class="col-xs-6 col-sm-3 col-md-2">
                                            <div class="category-list mtb-15">
                                                <a href="{{url('/category/'.$is_featured->slug)}}" class="product-effect">
                                                    @if($is_featured->image)
                                                        <img class="img-responsive"
                                                             src="{!! SM::sm_get_the_src($is_featured->image, 112,112) !!}"
                                                             alt="{{$is_featured->title}}">
                                                    @else
                                                        <img class="img-responsive"
                                                             src="{{ SM::sm_get_the_src(SM::sm_get_site_logo(), 112,112) }}"
                                                             alt="{{$is_featured->title}}">
                                                    @endif
                                                        <p class="title-color">{{$is_featured->title}}</p>

                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif