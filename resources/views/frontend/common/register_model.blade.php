

<div class="modal fade" id="onlyRegisterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
         
            <div class="modal-body model-body-padding">
                <?php
                $authCheck = Auth::check();
                ?>
                <div class="row">
        
                        <div class="col-md-6 border-right-log padding-model" >
                              <h4 class="text-center">POP IN YOUR DETAILS TO SAVE 30% ON GROCERIES!</h4>
                            {{ Form::open(['url' => ['/register'], 'id' => 'registrationForm', 'class'=>'smAuthForm']) }}
                            <div class="form-group">
                                <label for="username" class="col-form-label">Your Name:</label>
                                {!! Form::text('username', null, ['class' => 'form-control', 'required', 'id'=>'username', 'placeholder'=> 'Your Name . . .']) !!}
                                <span class="error-notice"></span>
                            </div>
                            <div class="form-group">
                                <label for="emmail_login" class="col-form-label">Email address:</label>
                                {!! Form::email('email', null, ['class' => 'form-control', 'required', 'id'=>'emmail_login', 'placeholder'=> 'Email Address . . .']) !!}
                                <span class="error-notice"></span>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-form-label">Password: (Minimum 6 charecters)</label>
                                <input id="password" minlength="6" type="password" name="password" required class="form-control " placeholder="Password">
                                <span class="error-notice"></span>
    
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation" minlength="6" class="col-form-label">Conform Password:</label>
                                <input id="password_confirmation" required name="password_confirmation" type="password"class="form-control" placeholder="Conform Password . . .">
                                <span class="error-notice"></span>
    
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-user"></i> Create an account
                                </button>
                            </div>
                            {!! Form::close() !!}
                          @include("frontend.common.register_social")
                    </div>
                     <div class="col-md-6">
                             <button type="button" class="login-cross-btn" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="hidden-xs">
                                 <a href="{{URL('/')}}" class="logologo">
                                    <img class="logo-style login-logo-style" alt="{{ SM::get_setting_value('site_name') }}" src="{{ SM::sm_get_the_src(SM::sm_get_site_logo(), 294, 90) }}" style="width:100%"/>
                                </a>
                               
                               <img src="{{asset('images/carts_generic.png')}}" class="right-banner">
                       </div>
                    </div>
                </div>
            </div>
           
        </div>
    </div>
</div>