<?php $special_product = SM::getSpecialProduct();
?>
@if(count($special_product)>0)
    <section class="site-content">
        <div class="container">
            <div class="products-area">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-spacial p-0">
                        <div class="paanel custom-panel-style">
                            <div class="panel-heading">
                                <h3 class="section-heading">Special Product</h3>
                            </div>
                            <div class="panel-body plr-0">
                                <!-- Tab panes -->
                                <div class="owl-carousel owl-loaded owl-drag">
                                        <div class="owl-stage-outer">
                                            <div class="owl-stage">
                                                @if(count($special_product)>0)
                                                    @forelse($special_product as $key=>$special_p)

                                                        @if($special_p->product_type==2)
                                                            <div class="owl-item active">
                                                                <div class="product border">
                                                                    <div class="product-grid">
                                                                        <div class="product-image">
                                                                            <a href="{{url('/frontend/product'.$special_p->slug)}}" class="image">
                                                                                 <img class=""  src="{!! SM::sm_get_the_src($special_p->image, 200,200) !!}" alt="{{$special_p->title}}">
                                                                            </a>

                                                                            <p class="custom-off-amount">{{SM::productDiscount($special_p->id )}}
                                                                                %</p>
                                                                        </div>
                                                                        <div class="product-content">
                                                                            <h3 class="title">
                                                                                <a href="Special Product ">{{$special_p->title}}</a>
                                                                            </h3>
                                                                            <h6 class="product-category">{{$special_p->title}}</h6>
                                                                            <a data-add_class="btn-add-cart"
                                                                               data-product_id="{{ $special_p->id }}"
                                                                               data-regular_price="{{ $special_p->regular_price }}"
                                                                               data-sale_price="{{ $special_p->sale_price }}"
                                                                               class="addToCart  add-to-cart-ex"><i
                                                                                        class="fa fa-shopping-cart cart-ad-custom">
                                                                                    Cart </i> </a>

                                                                            <div class="price">
                                                                                <span>৳{{$special_p->regular_price}}</span>৳{{$special_p->sale_price}}
                                                                            </div>
                                                                            <ul class="social">
                                                                                <li class="custom-heart-extra"><span products_id="80" class="fa  fa-heart-o  is_liked"></span>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="owl-item active">
                                                                <div class="product-grid mtb-15">
                                                                    <div class="product-image">
                                                                        <a href="{{url('/product/'.$special_p->slug)}}" class="image">
                                                                            <img class="pic-1" src="{!! SM::sm_get_the_src($special_p->image, 200,200) !!}">
                                                                        </a>
                                                                        {{--<span class="product-new-label">new</span>--}}
                                                                        <span class="product-discount-label">{{SM::productDiscount($special_p->id )}} %</span>
                                                                        {{--<ul class="social">--}}
                                                                            {{--<li><a href="#"><i class="fa fa-eye"></i></a></li>--}}
                                                                            {{--<li><a href="#"><i class="fa fa-heart"></i></a></li>--}}
                                                                        {{--</ul>--}}
                                                                    </div>
                                                                    <div class="product-content">
                                                                        <a href="{{url('/product/'.$special_p->slug)}}" class="title-color mb-1">{{$special_p->title}}</a>
                                                                        @if($special_p->sale_price > 0)
                                                                        <div class="price"><span>{{ SM::currency_price_value($special_p->regular_price) }} </span> {{ SM::currency_price_value($special_p->sale_price) }} </div>
                                                                        @else
                                                                            <div class="price"> {{ SM::currency_price_value($special_p->regular_price) }} </div>
                                                                        @endif
                                                                        <div class="product-link inc_dec_qty">
                                                                            <a href="#" class="weightunit"> {{$special_p->product_weight}} {{SM::product_wait_unit($special_p->unit_id)->title}}</a>
                                                                            <?php  echo SM::addToCartButton($special_p->id, $special_p->regular_price, $special_p->sale_price) ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @empty
                                                        <label class="label label-warning">Special Product Not
                                                            Found...!</label>
                                                    @endforelse
                                                @endif

                                            </div>
                                        </div>
                                    </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif

