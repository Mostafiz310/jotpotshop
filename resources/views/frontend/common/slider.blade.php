@if(isset($sliders) && count($sliders)>0)
    <?php
    $slider_change_autoplay = (int)SM::smGetThemeOption("slider_change_autoplay", 4);
    $slider_change_autoplay *= 3000;
    ?>


    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="3000">

        <div class="carousel-inner" role="listbox">
            @forelse($sliders as $key=>$slider)
                @if($key==0)
                    <div class="item active">
                @else
                    <div class="item">
                @endif
                    @if(!empty($slider->image))
                        <img src="{!! SM::sm_get_the_src($slider->background_image, 900,400) !!}" class="mx-auto" alt="{{$slider->title}}">
                    @endif
                    {{--<img src="//images.pexels.com/photos/147534/pexels-photo-147534.jpeg?w=1260&h=750&auto=compress&cs=tinysrgb" alt="Motocross Couple">--}}
                    {{--<div class="carousel-caption">--}}
                        {{--One-handed compact fold for storing and transporting--}}
                    {{--</div>--}}
                </div>
            @endforeach


            {{--<div class="item">--}}
                {{--<iframe width="100%" height="100%" src="//www.youtube.com/embed/jI-kpVh6e1U?controls=0&disablekb=1&enablejsapi=1&iv_load_policy=3&modestbranding=1&rel=0&playsinline=1" frameborder="0" allowfullscreen></iframe>--}}
                {{--<div class="carousel-caption">--}}
                    {{--Youtube--}}
                {{--</div>--}}
            {{--</div>--}}

        </div>

        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active">
                <i class="fa fa-stop" aria-hidden="true"></i>
            </li>
            <li data-target="#carousel-example-generic" data-slide-to="1">
                <i class="fa fa-stop" aria-hidden="true"></i>
            </li>
            <li data-target="#carousel-example-generic" data-slide-to="2" class="">
                <i class="fa fa-play" aria-hidden="true"></i>
            </li>
            <li data-target="#carousel-example-generic" data-slide-to="3">
                <i class="fa fa-stop" aria-hidden="true"></i>
            </li>
        </ol>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <i class="fa fa-chevron-right" aria-hidden="true"></i>
            <span class="sr-only">Next</span>
        </a>
    </div>

@endif
<script>
    $('#myCarousel').carousel({
        interval: 3000,
    })
</script>

