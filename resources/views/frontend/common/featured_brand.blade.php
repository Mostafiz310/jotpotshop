  <?php
    $features = SM::smGetThemeOption("features", array());
    ?>


  <section class="site-content">
      <div class="container">
          <div class="products-area">
              <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-spacial p-0">
                      <div class="paanel custom-panel-style">
                            <div class="panel-heading ">
                                <h3 class="section-heading">Popular Brand</h3>
                            </div>
                            <div class="panel-body plr-0">
                                <div class="row">
                                    <?php $features_brand = SM::getProductBrandsHome(0);?>
                                    @foreach($features_brand as $brands)
                                        <div class="col-6  col-sm-2  col-md-1 mtb-15">
                                            <a href="{{url('/brand/'.$brands->slug)}}">
                                                <img class="img-responsive brand-icon" src="{!! SM::sm_get_the_src($brands["image"], 118,83) !!}" alt="{{ $brands->title }}" title="{{ $brands->title }}">
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
