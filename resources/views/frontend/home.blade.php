@extends('frontend.master')
@section('title', '')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3">
                @include('frontend.common.sideber_category')
            </div>
            <div class="col-xs-12 col-sm-8 col-md-9">
                @include('frontend.common.slider')
            </div>
        </div>
        <div class="row">
            @include('frontend.common.category_list')
            @include('frontend.common.special_product')
            <section class="site-content">
                <div class="container">
                    <div class="products-area">
                        <?php $newest_product = SM::getNewestProduct(); ?>
                        <div class="paanel custom-panel-style">
                            <div class="panel-heading">
                                <h3 class="section-heading">Latest Product</h3>
                            </div>
                            <div class="panel-body plr-0">
                                <div id="special" role="tabpanel" aria-labelledby="special-tab">
                                    <div id="owl_special" class="owl-carousel owl-loaded owl-drag">
                                        <div class="owl-stage-outer">
                                            <div class="owl-stage">
                                                @forelse($newest_product as $product)

                                                    <div class="owl-item active" style="width: 268px; margin-right: 10px;">
                                                        <div class="product hidden">
                                                            <div class="product-grid">
                                                                <div class="product-image">
                                                                    <a href="{{url('/product/'.$product->slug)}}"
                                                                       class="image">
                                                                        <img class=""  src="{!! SM::sm_get_the_src($product->image, 200,200) !!}" alt="{{$product->title}}">

                                                                    </a>

                                                                    <p class="custom-off-amount">{{SM::productDiscount($product->id )}} %</p>

                                                                </div>
                                                                <div class="product-content">
                                                                    <h3 class="title"><a href="#">{{$product->title}}</a> </h3>
                                                                    @if($product->sale_price > 0)
                                                                        <p class="price">
                                                                            <span>{{ SM::currency_price_value($product->regular_price) }}</span>{{ SM::currency_price_value($product->sale_price) }}
                                                                        </p>
                                                                    @else
                                                                        <p class="price">{{ SM::currency_price_value($product->regular_price) }}</p>
                                                                    @endif
                                                                    <div class="btn-group custom-heart-extra">
                                                                        <button type="button" class="btn btn-warning Bold">{{$product->product_weight}} {{SM::product_wait_unit($product->unit_id)->title}}</button>
                                                                        <?php  echo SM::addToCartButton($product->id, $product->regular_price, $product->sale_price) ?>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="product-grid mtb-15">
                                                            <div class="product-image">
                                                                <a href="{{url('/product/'.$product->slug)}}" class="image">
                                                                    <img class="pic-1" src="{!! SM::sm_get_the_src($product->image, 200,200) !!}">
                                                                </a>
                                                                <span class="product-new-label">new</span>
                                                                <span class="product-discount-label">{{SM::productDiscount($product->id )}} %</span>
                                                                {{--<ul class="social">--}}
                                                                    {{--<li><a href="#"><i class="fa fa-eye"></i></a></li>--}}
                                                                    {{--<li><a href="#"><i class="fa fa-heart"></i></a></li>--}}
                                                                {{--</ul>--}}
                                                            </div>
                                                            <div class="product-content">
                                                                <a href="{{url('/product/'.$product->slug)}}" class="title-color mb-1">{{$product->title}}</a>
                                                                @if($product->sale_price > 0)
                                                                    <div class="price"><span>{{ SM::currency_price_value($product->regular_price) }} </span> {{ SM::currency_price_value($product->sale_price) }} </div>
                                                                @else
                                                                    <div class="price"> {{ SM::currency_price_value($product->regular_price) }} </div>
                                                                @endif
                                                                <div class="product-link inc_dec_qty">
                                                                    <a href="#" class=""> {{$product->product_weight}} {{SM::product_wait_unit($product->unit_id)->title}}</a>
                                                                    <?php  echo SM::addToCartButton($product->id, $product->regular_price, $product->sale_price) ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @empty
                                                    <label class="label label-warning">Newest Product Not Found ..!</label>
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
                @include('frontend.common.top_sale_product')
                @include('frontend.common.featured_brand')
                @include('frontend.common.blog')
            </div>
        </div>
    </div>

@endsection


