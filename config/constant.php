<?php

$asset = (PHP_SAPI === 'cli') ? false : asset('/');
$site = (PHP_SAPI === 'cli') ? false : url('/');
return [
    'smSite' => $site,
//admin slug and url
    'smAdminSlug' => 'admin',
    'smAdminUrl' => $site . '/admin/',
//pagination
    'smPagination' => 10,
    'smPaginationMedia' => 49,
    'smFrontPagination' => 10,
    'cachingTimeInMinutes' => 10,
    'popupHideTimeInMinutes' => 24 * 60,
    'popupHideTimeInMinutesForSubscriber' => 30 * 24 * 60,
//image upload directory and url
    'smUploadsDir' => 'uploads/',
    'smUploads' => $asset . 'uploads/',
    'smUploadsUrl' => $asset . 'uploads/',
//image size: width and height
//1: logo
//2-4:gallery
//5:manage page
//6:manage page
//7:author small
//8-10:blog
//11-11: sliders
//12 team
//13 testimonial logo
    'smPostMaxInMb' => 5,
//galary (600x400, 112x112 not crop resized)
    'smImgWidth' => [
        30, // fav icon-
        294, //header logo-
//        170, //Category list category icon
        118, //Brand icon
        418, //slider image
        900, //slider background image
        200, //product image
        270, //Sideber Adds1
        540, //Sideber Adds2
        250, //Shop page product image
        500, //product Details
        100, //product cart small
        270, //home page blog image
        369, //blog home page image
        358, //blog home page image
        748, //blog details image
        44, //Home Sider Bar
        //        -------admin panel-----
        165, //featured-image
        112, //media small image
        80, //lists image
        600,
//        539,//blogs,
//        580,
//        395,
    ],
    'smImgHeight' => [
        30, // fav icon-
        90, //header logo-
//        116, //Category list category icon
        83, //Brand icon
        366, //slider image
        400, //slider background image
        200, //product image
        450, //Sideber Adds1
        180, //Sideber Adds2
        250, //Shop page product image
        500, //product Details
        122, //product cart small
        170, //home page blog image
        258, //blog home page image
        200, //blog home page image
        436, //blog details image
        42, //Home Sider Bar
//        //        -------admin panel-----
        165, //featured-image-
        112, //media small image-
        80, //lists image-
        400,
//        300,//blog,
//        580,
//        330,
    ],
    //               1    2    3    4     5   6   7    8    9    10  11  12    13   14   15   16  17
];
