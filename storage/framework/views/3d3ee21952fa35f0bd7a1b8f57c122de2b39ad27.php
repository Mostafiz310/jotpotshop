<?php
$product_best_sale_is_enable = SM::smGetThemeOption("product_best_sale_is_enable", 1);
$product_show_category = SM::smGetThemeOption("product_show_category", 1);
$product_show_tag = SM::smGetThemeOption("product_show_tag", 1);
$product_show_brand = SM::smGetThemeOption("product_show_brand", 1);
$product_show_size = SM::smGetThemeOption("product_show_size", 1);
$product_show_color = SM::smGetThemeOption("product_show_color", 1);
$product_detail_add = SM::smGetThemeOption("product_detail_add", 1);
?>

<div class="column col-xs-12 col-sm-3 col-md-2 p-0" id="left_column">

    <!-- block category -->
    <?php if($product_show_category==1): ?>
        <?php
        $getMainCategories = SM::getProductCategories(0);
        ?>
        <?php if(count($getMainCategories)>0): ?>
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h4 class="title collapsed" href="#collapseAccordian1" data-toggle="collapse" aria-expanded="true"
                        aria-controls="collapseAccordian1">Categories </h4>
                </div>
                <div class="block_content">
                    <!-- layered -->
                    <div class="layered layered-category">
                        <div class="layered-content">
                            <ul class="check-box-list">
                                <?php $__currentLoopData = $getMainCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>
                                        <a href="<?php echo url("category/".$cat->slug); ?>" class="details-cat"><?php echo e($cat->title); ?></a>
                                        <?php
                                        $getSubCategories = SM::getProductsubCategories($cat->id);
                                        ?>
                                        <?php if(empty(!$getSubCategories)): ?>
                                            <ul style="display: block;">
                                                <?php $__currentLoopData = $getSubCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getSubCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if(count($getSubCategory->products)>0): ?>
                                                        <li><span></span>
                                                            <a style="color: #ff3366;"
                                                               href="<?php echo url("category/".$getSubCategory->slug); ?>"><?php echo e($getSubCategory->title); ?></a>
                                                            <?php
                                                            $getSubSubCategories = SM::getProductsubCategories($getSubCategory->id);
                                                            ?>
                                                            <?php if(empty(!$getSubSubCategories)): ?>
                                                                <ul style="display: block;">
                                                                    <?php $__currentLoopData = $getSubSubCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getSubSubCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <li><span></span>
                                                                            <a style="color: #ff3366;"
                                                                               href="<?php echo url("category/".$getSubSubCategory->slug); ?>"><?php echo e($getSubSubCategory->title); ?></a>

                                                                        </li>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                </ul>
                                                            <?php endif; ?>
                                                        </li>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    </div>
                    <!-- ./layered -->
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
<!-- ./block category  -->

    <?php
    $product_detail_add_link = SM::smGetThemeOption("product_detail_add_link", "#");
    $product_detail_add = SM::smGetThemeOption("product_detail_add");
    ?>
    <?php if(empty(!$product_detail_add)): ?>
        <div class="col-left-slide left-module">
            <div class="banner-opacity">
                <a href="<?php echo $product_detail_add_link; ?>">
                    <img src="<?php echo SM::sm_get_the_src( $product_detail_add, 319,319 ); ?>" alt="ads-banner"
                         class="image-style"></a>
            </div>
        </div>
<?php endif; ?>
<!--./left silde-->
</div>
<style>
    .content-cat-ex {
        padding: 0px -14px;
        height: 1000px;
        overflow-y: scroll;
    }
</style>