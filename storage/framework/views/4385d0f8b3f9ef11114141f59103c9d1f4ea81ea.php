<nav id="categories" class="navbar navbar-expand-lg p-0 categories">
    <ul class="sidbar-category flex-column">
            <li class="category-text">All Categories</li>
            <?php $category = SM::getProductCategories(); ?>
            <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                
                <?php $sub_category = SM::getProductsubCategories($category_list->id);?>

                <li class="nav-item dropdown">
                    <a href="<?php echo e(url('/category/'.$category_list->slug)); ?>" class="nav-link dropdown-toggle">
                        <img class="img-fuild"
                             src="<?php echo SM::sm_get_the_src($category_list->image, 30,30); ?>"> <?php echo e($category_list->title); ?>

                        <?php if (count($sub_category) > 0) { ?>
                        <i class="fa fa-angle-right " aria-hidden="true"></i>
                        <?php } ?>
                    </a>

                    <?php if(count($sub_category)>0): ?>
                        <ul class="dropdown-menu multi-level ">
                            <?php $__currentLoopData = $sub_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_categort_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(count($sub_categort_list->products) > 0): ?>
                                    <?php $sub__sub_category = SM::getProductsubCategories($sub_categort_list->id); ?>
                                    <li class="dropdown-submenu">
                                        <a class="dropdown-item" tabindex="-1"
                                           href="<?php echo e(url('/category/'.$sub_categort_list->slug)); ?>">
                                            <?php echo e($sub_categort_list->title); ?>

                                            <?php if (count($sub__sub_category) > 0) { ?>
                                            <i class="fa fa-angle-right " aria-hidden="true"></i>
                                            <?php } ?>
                                        </a>
                                        <?php if(count($sub__sub_category)>0): ?>
                                            <ul class="dropdown-menu multi-level ">
                                                <?php $__currentLoopData = $sub__sub_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_sub_categort_list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    
                                                    <li class="dropdown-submenu">
                                                        <a class="dropdown-item" tabindex="-1"
                                                           href="<?php echo e(url('/category/'.$sub_sub_categort_list->slug)); ?>">
                                                            <?php echo e($sub_sub_categort_list->title); ?>

                                                        </a>
                                                    </li>
                                                    
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        <?php endif; ?>

                                    </li>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    <?php endif; ?>
                </li>
                
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
</nav>

 <?php
    $home_left_image_link = SM::smGetThemeOption("home_left_image_link", "#");
    $home_left_image = SM::smGetThemeOption("home_left_image");
    $home_left_image_enable = SM::smGetThemeOption("home_left_image_enable", 1);
     ?> 
 <?php if($home_left_image_enable==1): ?>
        <?php if(empty(!$home_left_image)): ?>
        <div class="panel panel-default panel-extra hidden-sm hidden">
            <a href="<?php echo $home_left_image_link; ?>" title="Fresh Vegetable">
                <div class="panel-body box-img">
                    <img src="<?php echo SM::sm_get_the_src( $home_left_image, 270,450 ); ?>" alt="ads-banner" title="ads-banner" style=" width: 100%;">
                </div>
            </a>
        </div>
         <?php endif; ?>
 <?php endif; ?>
