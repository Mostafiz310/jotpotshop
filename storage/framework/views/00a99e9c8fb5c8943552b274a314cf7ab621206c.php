<?php $__env->startSection('title', 'Shop'); ?>
<?php $__env->startSection('content'); ?>
    <?php
    $countStickyPost = count($stickyProductPost);
    $isBreadcrumbEnable = SM::smGetThemeOption("product_is_breadcrumb_enable", false);
    $pagination = [
        [
            "title" => "Product"
        ]
    ];
    $title = SM::smGetThemeOption("product_banner_title");
    $subtitle = SM::smGetThemeOption("product_banner_subtitle");
    $bannerImage = SM::smGetThemeOption("product_banner_image");
    ?>

    <div class="columns-container">
        <div class="container" id="columns">
            <!-- breadcrumb -->
        <?php echo $__env->make('frontend.common.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- ./breadcrumb -->
            <!-- row -->
            <div class="row">
            <?php echo $__env->make('frontend.products.product_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="center_column col-xs-12 col-md-10 col-lg-10 col-sm-9" id="center_column">
                    <!-- view-product-list-->
                    <div class="short-list-style">
                        <div class="row">
                            <div class="form-group col-xs-12 col-md-9 mb-0">
                                <h5 class="title-style">
                                    <i class="fa fa-shopping-cart"></i> <span id="category_filter_data"><strong>Shop</strong></span>
                                    <span id="brand_filter_data"></span>
                                </h5>
                            </div>
                            <div class="form-group col-xs-12 col-md-3 mb-0">
                                <div class="input-group mb-0">
                                    <label class="input-group-addon">Sort By</label>
                                    <select data-role="sorter" class="form-control sortby onChangeProductFilter orderByPrice">
                                        <option value="">Popularity</option>
                                        <option value="1">Product Name</option>
                                        <option value="2">New</option>
                                        <option value="3">Best Sellers</option>
                                        <option value="4">Most Viewed</option>
                                        <option value="5">Price low to high</option>
                                        <option value="6">Price high to low</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="ajax_view_product_list">
                    </div>
                </div>
                <!-- ./view-product-list-->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>