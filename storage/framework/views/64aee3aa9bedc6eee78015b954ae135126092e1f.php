<div class="account-panel account-sidebar">
    <h2>My Account</h2>
    <?php echo Form::open(['method'=>'post', 'url'=>'dashboard/user-profile-pic-change','files'=>true, 'id'=>'profile_picture_form']); ?>

    <div class="account-profile-img">
        <img id="profile_picture_img" src="<?php echo SM::sm_get_the_src($userInfo->image,165,165); ?>"
             alt="<?php echo e($userInfo->username); ?>">
        <span class="change-profile-pic">
                <i class="fa fa-camera"></i>
                 <input type="file" name="profile_picture">
        </span>
    </div>
    <?php echo Form::close(); ?>

    <h4 class="change-account-name-opt">
        <?php
        $flname = $userInfo->firstname . " " . $userInfo->lastname;
        $name = trim($flname != '') ? $flname : $userInfo->username;
        ?>
        <?php echo e($name); ?>

        <a href="<?php echo url("dashboard/edit-profile"); ?>"><i class="fa fa-pencil-square-o"></i> </a>
    </h4>
    <span class="devider"></span>
    <ul>
        <li class="<?php if($activeMenu == 'dashboard'): ?> <?php echo e('active'); ?> <?php endif; ?>">
            <a href="<?php echo url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard
            </a>
        </li>
        <li class="<?php if($activeMenu == 'edit-profile'): ?> <?php echo e('active'); ?> <?php endif; ?>">
            <a href="<?php echo url("dashboard/edit-profile"); ?>">
                <i class="fa fa-file-text-o"></i> Personal Information
            </a>
        </li>
        <li class="<?php if($activeMenu == 'orders'): ?> <?php echo e('active'); ?> <?php endif; ?>">
            <a href="<?php echo url('dashboard/orders'); ?>"><i class="fa fa-shopping-cart"></i> My Orders
            </a>
        </li>
        <li style="display: none;" class="<?php if($activeMenu == 'wishlist'): ?> <?php echo e('active'); ?> <?php endif; ?>">
            <a href="<?php echo url('dashboard/wishlist'); ?>"><i class="fa fa-heart"></i> Wishlist
            </a>
        </li>
        <li class="<?php if($activeMenu == 'review'): ?> <?php echo e('active'); ?> <?php endif; ?>">
            <a href="<?php echo url('dashboard/review'); ?>"><i class="fa fa-star"></i> Review
            </a>
        </li>

        
        
        
        
        <li style="display: none;" class="<?php if($activeMenu == 'downloads'): ?> <?php echo e('active'); ?> <?php endif; ?>">
            <a href="<?php echo url('dashboard/downloads'); ?>"><i class="fa fa-download"></i> Downloads
            </a>
        </li>
        <li style="display: none;" class="<?php if($activeMenu == 'tickets'): ?> <?php echo e('active'); ?> <?php endif; ?>">
            <a href="<?php echo url('dashboard/tickets'); ?>"><i class="fa fa-support"></i> My Tickets
            </a>
        </li>
        <li style="display: none;" class="<?php if($activeMenu == 'add-ticket'): ?> <?php echo e('active'); ?> <?php endif; ?>">
            <a href="<?php echo url('dashboard/tickets/add'); ?>"><i class="fa fa-ticket"></i> Add Tickets
            </a>
        </li>
        <li>
            <a href="<?php echo url('logout'); ?>"><i class="fa fa-power-off"></i> Log Out</a>
        </li>
    </ul>
</div>
