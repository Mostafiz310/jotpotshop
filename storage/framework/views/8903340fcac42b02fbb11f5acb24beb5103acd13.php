<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php echo $__env->make('frontend.common.meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('frontend.inc.css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('frontend.common.additional_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldPushContent('style'); ?>
<!--------- stripe js ------>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script><style>
    .col-md-7.col-12.order-md-1.order-2 {
padding-top: 112px;
}
</style>
</head>
<body>
<div id="fb-root"></div>
<?php echo $__env->make('frontend.common.only_login_Modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('frontend.common.register_model', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('frontend.common.login_modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('frontend.inc.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="container">
    <div class="row">
            <?php echo $__env->make('frontend.common.s_w_message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="search-html">
                <?php echo $__env->yieldContent('content'); ?>
            </div>
      </div>
</div>
<?php echo $__env->make('frontend.inc.footer_top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('frontend.inc.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- all js scripts including custom js -->
<!-- scripts -->
<?php echo $__env->make('frontend.inc.js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('frontend.common.additional_js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldPushContent('script'); ?>


</body>
</html>


