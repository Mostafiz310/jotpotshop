<?php
$site_name = SM::sm_get_site_name();
$site_name = SM::sm_string($site_name) ? $site_name : 'buckleup-bd';
$mobile = SM::get_setting_value('mobile');
$email = SM::get_setting_value('email');
$address = SM::get_setting_value('address');
$footer_logo = SM::smGetThemeOption("footer_logo", "");
$footer_widget2_title = SM::smGetThemeOption('footer_widget2_title', "Seo Services");
$footer_widget2_description = SM::smGetThemeOption('footer_widget2_description', "");
$footer_widget3_title = SM::smGetThemeOption('footer_widget3_title', "Company");
$footer_widget3_description = SM::smGetThemeOption('footer_widget3_description', "");
$footer_widget4_title = SM::smGetThemeOption('footer_widget4_title', "Technology");
$footer_widget4_description = SM::smGetThemeOption('footer_widget4_description', "");
$contact_branches = SM::smGetThemeOption("contact_branches");
$newsletter_success_title = SM::smGetThemeOption("newsletter_success_title", "Thank You For Subscribing!");
$newsletter_success_description = SM::smGetThemeOption("newsletter_success_description", "You're just one step away from being one of our dear susbcribers.Please check the Email provided and confirm your susbcription.");
$payment_method_image = SM::smGetThemeOption("payment_method_image", "");
?>


<footer class="footer-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="single-footer ptb-15">
                    <a href="<?php echo e(URL('/')); ?>" class="logo">
                        <img class="img-responsive" alt="<?php echo e(SM::get_setting_value('site_name')); ?>" src="<?php echo e(SM::sm_get_the_src(SM::sm_get_site_logo(), 294, 90)); ?>"/>
                    </a>
                    <h4>Contact info</h4>
                    <ul class="contact-list">
                        <li><i class="fa fa-map-marker"> </i> <?php echo e($address); ?></li>
                        <li><i class="fa fa-phone"> </i> <?php echo e($mobile); ?></li>
                        <li><i class="fa fa-envelope-open-o"> </i> <?php echo e($email); ?></li>

                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="footer-block">
                    <div class="footer-menu ptb-15">
                        <h4 class="widget-title"><?php echo e($footer_widget2_title); ?></h4>
                        <?php echo stripslashes($footer_widget2_description); ?>

                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="footer-block">
                    <div class="footer-menu ptb-15">
                        <h4><?php echo e($footer_widget3_title); ?></h4>
                        <?php echo stripslashes($footer_widget3_description); ?>

                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="single-footer ptb-15">
                    <h4>Subscribe Now</h4>
                    <?php echo Form::open(["method"=>"post", "action"=>'Front\HomeController@subscribe', 'class'=>'form-inline form-subscribe', 'id'=>"newsletterForm"]); ?>

                    <div class="input-group">
                        <input name="email" required type="email" class="form-control"
                               placeholder="Enter Your E-mail Address" style=" width: 80%;">
                        <button type="submit" value="Subscribe" id="newsletterFormSubmit"
                                class="btn btn-success input-group-addon"><i class="fa fa-paper-plane-o"></i>
                        </button>
                    </div>

                    <?php echo Form::close(); ?>


                    <div class="socials">
                        <h5>Follow Us</h5>
                        <ul class="list">
                            <?php if(empty(!SM::smGetThemeOption("social_facebook"))): ?>
                                <li><a target="_blank" href="<?php echo e(SM::smGetThemeOption("social_facebook")); ?>">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if(empty(!SM::smGetThemeOption("social_twitter"))): ?>
                                <li><a target="_blank" href="<?php echo e(SM::smGetThemeOption("social_twitter")); ?>">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if(empty(!SM::smGetThemeOption("social_google_plus"))): ?>
                                <li><a target="_blank" href="<?php echo e(SM::smGetThemeOption("social_google_plus")); ?>">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if(empty(!SM::smGetThemeOption("social_linkedin"))): ?>
                                <li><a target="_blank" href="<?php echo e(SM::smGetThemeOption("social_linkedin")); ?>">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if(empty(!SM::smGetThemeOption("social_github"))): ?>
                                <li><a target="_blank" href="<?php echo e(SM::smGetThemeOption("social_github")); ?>">
                                        <i class="fa fa-github"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if(empty(!SM::smGetThemeOption("social_pinterest"))): ?>
                                <li><a target="_blank" href="<?php echo e(SM::smGetThemeOption("social_pinterest")); ?>">
                                        <i class="fa fa-pinterest-p"> </i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if(empty(!SM::smGetThemeOption("social_youtube"))): ?>
                                <li><a target="_blank" href="<?php echo e(SM::smGetThemeOption("social_youtube")); ?>">
                                        <i class="fa fa-youtube-play"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>

<div class="footer py-2 my-2" style="background: #630767;color: #fff;">
    <div class="container-fluid">
        <div class="row">
            <div class="footer-image col-12 col-md-6">
                
            </div>
            <div class="footer-info col-12 col-md-6">
                <p class="text-center"><?php echo e(SM::smGetThemeOption("copyright")); ?>

                    <a style="color: #fff;" target="_blank" href="http://nextpagetl.com/">Next Page Technology
                        Ltd.</a>
                </p>
            </div>
        </div>
    </div>
</div>

<!--notification-->
<div id="message_content"></div>