<?php
$productSecondLoop = 1;
?>
<?php if(count($productLists) > 0): ?>
<?php $__currentLoopData = $productLists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if($product->product_type==2): ?>
<?php
$att_data = SM::getAttributeByProductId($product->id);
if (!empty($att_data->attribute_image)) {
    $attribute_image = $att_data->attribute_image;
} else {
    $attribute_image = $product->image;
}
?>

<li class="col-sx-12 col-sm-4">
    <!-- bootstrap carousel -->
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"
         data-interval="false">
        <!-- Wrapper for slides -->
        <div class="carousel-inner sp-wrap">
            <div class="item active srle">
                <a href="<?php echo e(url('product/'.$product->slug)); ?>">
                    <img src="<?php echo e(SM::sm_get_the_src($attribute_image, 303, 455)); ?>" alt="<?php echo e($product->title); ?>" class="img-responsive">
                </a>
            </div>

            <?php
            $myString = $product->image_gallery;
            $myArray = explode(',', $myString);

            ?>
            <?php $__currentLoopData = $myArray; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v_data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="item">
                    <a href="<?php echo e(url('product/'.$product->slug)); ?>">
                        <img src="<?php echo SM::sm_get_the_src( $v_data, 303, 455); ?>" alt="<?php echo e($product->title); ?>" class="img-responsive">
                    </a>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <div class="new-tag">
                <img src="<?php echo e(asset('/frontend')); ?>/image/new.png">
            </div>
        </div>
        <a href="" class="wishlist">
            <i class="fa fa-heart"></i>
        </a>
        <div class="">
            
            <h5 class="product-name"><a href="<?php echo e(url('product/'.$product->slug)); ?>"><?php echo e($product->title); ?></a></h5>
            <div class="text-center parice-margin-left">
                <span class="price product-price"><?php echo e(SM::currency_price_value($att_data->attribute_price)); ?></span>
            </div>
        </div>

        <div class="price-percentage-product">
            -30% OFF
        </div>
    </div>
</li>
<?php else: ?>
    <li class="col-sx-12 col-sm-4">
        <div class="product-grid mtb-15">
            <div class="product-image">
                <a href="<?php echo e(url('/product/'.$product->slug)); ?>" class="image">
                    <img class="pic-1" src="<?php echo SM::sm_get_the_src($product->image, 200,200); ?>">
                </a>
                
                <span class="product-discount-label"><?php echo e(SM::productDiscount($product->id )); ?> %</span>
                
                
                
                
            </div>
            <div class="product-content">
                <a href="<?php echo e(url('/product/'.$product->slug)); ?>" class="title-color mb-1"><?php echo e($product->title); ?></a>
                <?php if($product->sale_price > 0): ?>
                    <div class="price"><span><?php echo e(SM::currency_price_value($product->regular_price)); ?> </span> <?php echo e(SM::currency_price_value($product->sale_price)); ?> </div>
                <?php else: ?>
                    <div class="price"> <?php echo e(SM::currency_price_value($product->regular_price)); ?> </div>
                <?php endif; ?>
                <div class="product-link inc_dec_qty">
                    <a href="#" class=""> <?php echo e($product->product_weight); ?> <?php echo e(SM::product_wait_unit($product->unit_id)->title); ?></a>
                    <?php  echo SM::addToCartButton($product->id, $product->regular_price, $product->sale_price) ?>
                </div>
            </div>
        </div>
    </li>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php else: ?>
<div class="alert alert-info"><i class="fa fa-info"></i> No Product Found!</div>
<?php endif; ?>
<div class="col-md-12" style="margin-top: 25px;">
    <?php echo $productLists->links(); ?>

</div>
