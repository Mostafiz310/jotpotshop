  <?php
    $features = SM::smGetThemeOption("features", array());
    ?>


  <section class="site-content">
      <div class="container">
          <div class="products-area">
              <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-spacial p-0">
                      <div class="paanel custom-panel-style">
                            <div class="panel-heading ">
                                <h3 class="section-heading">Popular Brand</h3>
                            </div>
                            <div class="panel-body plr-0">
                                <div class="row">
                                    <?php $features_brand = SM::getProductBrandsHome(0);?>
                                    <?php $__currentLoopData = $features_brand; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brands): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-6  col-sm-2  col-md-1 mtb-15">
                                            <a href="<?php echo e(url('/brand/'.$brands->slug)); ?>">
                                                <img class="img-responsive brand-icon" src="<?php echo SM::sm_get_the_src($brands["image"], 118,83); ?>" alt="<?php echo e($brands->title); ?>" title="<?php echo e($brands->title); ?>">
                                            </a>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
