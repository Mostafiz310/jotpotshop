<?php if(isset($sliders) && count($sliders)>0): ?>
    <?php
    $slider_change_autoplay = (int)SM::smGetThemeOption("slider_change_autoplay", 4);
    $slider_change_autoplay *= 3000;
    ?>


    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="3000">

        <div class="carousel-inner" role="listbox">
            <?php $__empty_1 = true; $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <?php if($key==0): ?>
                    <div class="item active">
                <?php else: ?>
                    <div class="item">
                <?php endif; ?>
                    <?php if(!empty($slider->image)): ?>
                        <img src="<?php echo SM::sm_get_the_src($slider->background_image, 900,400); ?>" class="mx-auto" alt="<?php echo e($slider->title); ?>">
                    <?php endif; ?>
                    
                    
                        
                    
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


            
                
                
                    
                
            

        </div>

        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active">
                <i class="fa fa-stop" aria-hidden="true"></i>
            </li>
            <li data-target="#carousel-example-generic" data-slide-to="1">
                <i class="fa fa-stop" aria-hidden="true"></i>
            </li>
            <li data-target="#carousel-example-generic" data-slide-to="2" class="">
                <i class="fa fa-play" aria-hidden="true"></i>
            </li>
            <li data-target="#carousel-example-generic" data-slide-to="3">
                <i class="fa fa-stop" aria-hidden="true"></i>
            </li>
        </ol>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <i class="fa fa-chevron-right" aria-hidden="true"></i>
            <span class="sr-only">Next</span>
        </a>
    </div>

<?php endif; ?>
<script>
    $('#myCarousel').carousel({
        interval: 3000,
    })
</script>

