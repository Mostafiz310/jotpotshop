<?php $special_product = SM::getSpecialProduct();
?>
<?php if(count($special_product)>0): ?>
    <section class="site-content">
        <div class="container">
            <div class="products-area">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-spacial p-0">
                        <div class="paanel custom-panel-style">
                            <div class="panel-heading">
                                <h3 class="section-heading">Special Product</h3>
                            </div>
                            <div class="panel-body plr-0">
                                <!-- Tab panes -->
                                <div class="owl-carousel owl-loaded owl-drag">
                                        <div class="owl-stage-outer">
                                            <div class="owl-stage">
                                                <?php if(count($special_product)>0): ?>
                                                    <?php $__empty_1 = true; $__currentLoopData = $special_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$special_p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>

                                                        <?php if($special_p->product_type==2): ?>
                                                            <div class="owl-item active">
                                                                <div class="product border">
                                                                    <div class="product-grid">
                                                                        <div class="product-image">
                                                                            <a href="<?php echo e(url('/frontend/product'.$special_p->slug)); ?>" class="image">
                                                                                 <img class=""  src="<?php echo SM::sm_get_the_src($special_p->image, 200,200); ?>" alt="<?php echo e($special_p->title); ?>">
                                                                            </a>

                                                                            <p class="custom-off-amount"><?php echo e(SM::productDiscount($special_p->id )); ?>

                                                                                %</p>
                                                                        </div>
                                                                        <div class="product-content">
                                                                            <h3 class="title">
                                                                                <a href="Special Product "><?php echo e($special_p->title); ?></a>
                                                                            </h3>
                                                                            <h6 class="product-category"><?php echo e($special_p->title); ?></h6>
                                                                            <a data-add_class="btn-add-cart"
                                                                               data-product_id="<?php echo e($special_p->id); ?>"
                                                                               data-regular_price="<?php echo e($special_p->regular_price); ?>"
                                                                               data-sale_price="<?php echo e($special_p->sale_price); ?>"
                                                                               class="addToCart  add-to-cart-ex"><i
                                                                                        class="fa fa-shopping-cart cart-ad-custom">
                                                                                    Cart </i> </a>

                                                                            <div class="price">
                                                                                <span>৳<?php echo e($special_p->regular_price); ?></span>৳<?php echo e($special_p->sale_price); ?>

                                                                            </div>
                                                                            <ul class="social">
                                                                                <li class="custom-heart-extra"><span products_id="80" class="fa  fa-heart-o  is_liked"></span>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        <?php else: ?>
                                                            <div class="owl-item active">
                                                                <div class="product-grid mtb-15">
                                                                    <div class="product-image">
                                                                        <a href="<?php echo e(url('/product/'.$special_p->slug)); ?>" class="image">
                                                                            <img class="pic-1" src="<?php echo SM::sm_get_the_src($special_p->image, 200,200); ?>">
                                                                        </a>
                                                                        
                                                                        <span class="product-discount-label"><?php echo e(SM::productDiscount($special_p->id )); ?> %</span>
                                                                        
                                                                            
                                                                            
                                                                        
                                                                    </div>
                                                                    <div class="product-content">
                                                                        <a href="<?php echo e(url('/product/'.$special_p->slug)); ?>" class="title-color mb-1"><?php echo e($special_p->title); ?></a>
                                                                        <?php if($special_p->sale_price > 0): ?>
                                                                        <div class="price"><span><?php echo e(SM::currency_price_value($special_p->regular_price)); ?> </span> <?php echo e(SM::currency_price_value($special_p->sale_price)); ?> </div>
                                                                        <?php else: ?>
                                                                            <div class="price"> <?php echo e(SM::currency_price_value($special_p->regular_price)); ?> </div>
                                                                        <?php endif; ?>
                                                                        <div class="product-link inc_dec_qty">
                                                                            <a href="#" class="weightunit"> <?php echo e($special_p->product_weight); ?> <?php echo e(SM::product_wait_unit($special_p->unit_id)->title); ?></a>
                                                                            <?php  echo SM::addToCartButton($special_p->id, $special_p->regular_price, $special_p->sale_price) ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                        <label class="label label-warning">Special Product Not
                                                            Found...!</label>
                                                    <?php endif; ?>
                                                <?php endif; ?>

                                            </div>
                                        </div>
                                    </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

