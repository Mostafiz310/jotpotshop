<?php $__env->startSection('title', ''); ?>
<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3">
                <?php echo $__env->make('frontend.common.sideber_category', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-9">
                <?php echo $__env->make('frontend.common.slider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>
        <div class="row">
            <?php echo $__env->make('frontend.common.category_list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('frontend.common.special_product', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <section class="site-content">
                <div class="container">
                    <div class="products-area">
                        <?php $newest_product = SM::getNewestProduct(); ?>
                        <div class="paanel custom-panel-style">
                            <div class="panel-heading">
                                <h3 class="section-heading">Latest Product</h3>
                            </div>
                            <div class="panel-body plr-0">
                                <div id="special" role="tabpanel" aria-labelledby="special-tab">
                                    <div id="owl_special" class="owl-carousel owl-loaded owl-drag">
                                        <div class="owl-stage-outer">
                                            <div class="owl-stage">
                                                <?php $__empty_1 = true; $__currentLoopData = $newest_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>

                                                    <div class="owl-item active" style="width: 268px; margin-right: 10px;">
                                                        <div class="product hidden">
                                                            <div class="product-grid">
                                                                <div class="product-image">
                                                                    <a href="<?php echo e(url('/product/'.$product->slug)); ?>"
                                                                       class="image">
                                                                        <img class=""  src="<?php echo SM::sm_get_the_src($product->image, 200,200); ?>" alt="<?php echo e($product->title); ?>">

                                                                    </a>

                                                                    <p class="custom-off-amount"><?php echo e(SM::productDiscount($product->id )); ?> %</p>

                                                                </div>
                                                                <div class="product-content">
                                                                    <h3 class="title"><a href="#"><?php echo e($product->title); ?></a> </h3>
                                                                    <?php if($product->sale_price > 0): ?>
                                                                        <p class="price">
                                                                            <span><?php echo e(SM::currency_price_value($product->regular_price)); ?></span><?php echo e(SM::currency_price_value($product->sale_price)); ?>

                                                                        </p>
                                                                    <?php else: ?>
                                                                        <p class="price"><?php echo e(SM::currency_price_value($product->regular_price)); ?></p>
                                                                    <?php endif; ?>
                                                                    <div class="btn-group custom-heart-extra">
                                                                        <button type="button" class="btn btn-warning Bold"><?php echo e($product->product_weight); ?> <?php echo e(SM::product_wait_unit($product->unit_id)->title); ?></button>
                                                                        <?php  echo SM::addToCartButton($product->id, $product->regular_price, $product->sale_price) ?>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="product-grid mtb-15">
                                                            <div class="product-image">
                                                                <a href="<?php echo e(url('/product/'.$product->slug)); ?>" class="image">
                                                                    <img class="pic-1" src="<?php echo SM::sm_get_the_src($product->image, 200,200); ?>">
                                                                </a>
                                                                <span class="product-new-label">new</span>
                                                                <span class="product-discount-label"><?php echo e(SM::productDiscount($product->id )); ?> %</span>
                                                                
                                                                    
                                                                    
                                                                
                                                            </div>
                                                            <div class="product-content">
                                                                <a href="<?php echo e(url('/product/'.$product->slug)); ?>" class="title-color mb-1"><?php echo e($product->title); ?></a>
                                                                <?php if($product->sale_price > 0): ?>
                                                                    <div class="price"><span><?php echo e(SM::currency_price_value($product->regular_price)); ?> </span> <?php echo e(SM::currency_price_value($product->sale_price)); ?> </div>
                                                                <?php else: ?>
                                                                    <div class="price"> <?php echo e(SM::currency_price_value($product->regular_price)); ?> </div>
                                                                <?php endif; ?>
                                                                <div class="product-link inc_dec_qty">
                                                                    <a href="#" class=""> <?php echo e($product->product_weight); ?> <?php echo e(SM::product_wait_unit($product->unit_id)->title); ?></a>
                                                                    <?php  echo SM::addToCartButton($product->id, $product->regular_price, $product->sale_price) ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                    <label class="label label-warning">Newest Product Not Found ..!</label>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
                <?php echo $__env->make('frontend.common.top_sale_product', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('frontend.common.featured_brand', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('frontend.common.blog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('frontend.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>