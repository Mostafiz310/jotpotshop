<?php $__env->startSection("title", "Orders"); ?>
<?php $__env->startSection("content"); ?>
    <style>
        .order-btn a {
            padding: 7px 15px;
        }
    </style>
    <section class="common-section bg-black">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <?php echo $__env->make("customer.left-sidebar", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <div class="col-sm-9">
                    <div class="account-panel">
                        <h2>Order <?php if(isset($status)): ?>
                                <?php if($status==1): ?>
                                    Completed
                                <?php elseif($status==2): ?>
                                    Progress
                                <?php elseif($status==3): ?>
                                    Pending
                                <?php else: ?>
                                    Canceled
                                <?php endif; ?>
                            <?php endif; ?> List
                        </h2>
                        <?php if(count($orders)>0): ?>
                            <div class="account-panel-inner">
                                <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="single-order">
                                        <div class="order-head clearfix">
                                            <h5 class="pull-left"><b>Order date:</b>
                                                <?php echo e(date('d-m-Y', strtotime($order->created_at))); ?></h5>
                                            <h5 class="pull-right"><b>Order ID:</b>
                                                <?php echo e($order->invoice_no); ?></h5>
                                        </div>
                                        <div class="order-content">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3>
                                                    </h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <p><b>Order
                                                            Total:</b> <?php echo e(SM::order_currency_price_value($order->id,$order->grand_total)); ?>

                                                    </p>
                                                    <p>
                                                        <b>Paid:</b> <?php echo e(SM::order_currency_price_value($order->id,$order->paid)); ?>

                                                    </p>
                                                <?php
                                                $due = $order->paid - $order->net_total;
                                                $dueSign = $due < 0 ? "-" : "+";
                                                $dueSign = $due == 0 ? "" : $dueSign;
                                                ?>
                                                <?php if($due<0): ?>
                                                    <!--<p><b>Due:</b> <?php echo e($dueSign); ?> $<?php echo e(abs($due)); ?></p>
                                            <a href="<?php echo e(url("dashboard/orders/pay/$order->id")); ?>"
                                               class="btn btn-warning">Pay your Due
                                            </a>-->
                                                    <?php endif; ?>
                                                    <div class="order-btn">
                                                        
                                                        
                                                        
                                                        <a title="View Invoice"
                                                           target="_blank"
                                                           href="<?php echo url("dashboard/orders/detail/$order->id"); ?>">
                                                            <i class="fa fa-eye"> </i>
                                                        </a>
                                                        
                                                        
                                                        <a title="Download Invoice"
                                                           href="<?php echo url("dashboard/orders/download/$order->id"); ?>"><i
                                                                    class="fa fa-download"></i> </a>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <p><b>Order Status:</b> <?php
                                                        if ($order->order_status == 1) {
                                                            echo 'Completed';
                                                        } else if ($order->order_status == 2) {
                                                            echo 'Processing';
                                                        } else if ($order->order_status == 3) {
                                                            echo 'Pending';
                                                        } else {
                                                            echo 'Cancel';
                                                        }
                                                        ?></p>
                                                    <p><b>Payment Status:</b> <?php
                                                        if ($order->payment_status == 1) {
                                                            echo 'Completed';
                                                        } else if ($order->payment_status == 2) {
                                                            echo 'Pending';
                                                        } else if ($order->payment_status == 3) {
                                                            echo 'Pending';
                                                        } else {
                                                            echo 'Cancel';
                                                        }
                                                        ?>
                                                    </p>

                                                    <?php
                                                    if (!empty(trim($order->completed_files))) {
                                                    $files = SM::getMediaArrayFromStringImages($order->completed_files);
                                                    if (count($files) > 0) {
                                                    ?>
                                                    <p><strong>Completed Files:</strong><br>
                                                    <?php
                                                    foreach ($files as $file) {
                                                        $filename = $file->slug;
                                                        $img = SM::sm_get_galary_src_data_img($filename, $file->is_private == 1 ? true : false);
                                                        $src = $img['src'];
                                                        $data_img = $img['data_img'];

                                                        echo '<a href="' . url('/dashboard/media/download/' . $file->id) . '" title="Download ' . $file->title . '">
													<img src="' . $src . '"
                                                     caption="' . $file->caption . '" description="' . $file->description . '"
                                                     class="orderFile"></a>';
                                                    }
                                                    ?>
                                                    <div class="clearfix"></div>
                                                    </p>
                                                    <?php
                                                    }
                                                    }
                                                    ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                            <div class="text-center">
                                <?php echo $orders->links('common.pagination_orders'); ?>

                            </div>
                        <?php else: ?>
                            <div class="alert alert-warning">
                                <i class="fa fa-warning"></i> No <?php if(isset($status)): ?>
                                    <?php if($status==1): ?>
                                        Completed
                                    <?php elseif($status==2): ?>
                                        Progress
                                    <?php elseif($status==3): ?>
                                        Pending
                                    <?php elseif($status==3): ?>
                                        Canceled
                                    <?php endif; ?>
                                <?php endif; ?> Order Found!
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>