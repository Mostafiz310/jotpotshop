<?php $is_featureds = SM::getIsFeaturedCategories(); ?>
<?php if(count($is_featureds)>0): ?>
    <section class="site-content">
        <div class="container">
            <div class="products-area">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-spacial p-0">
                        <div class="paanel custom-panel-style">
                            <div class="panel-heading">
                                <h3 class="section-heading">Product Category</h3>
                            </div>
                            <div class="panel-body plr-0">
                                <div class="row">
                                    <?php $__currentLoopData = $is_featureds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $is_featured): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-xs-6 col-sm-3 col-md-2">
                                            <div class="category-list mtb-15">
                                                <a href="<?php echo e(url('/category/'.$is_featured->slug)); ?>" class="product-effect">
                                                    <?php if($is_featured->image): ?>
                                                        <img class="img-responsive"
                                                             src="<?php echo SM::sm_get_the_src($is_featured->image, 112,112); ?>"
                                                             alt="<?php echo e($is_featured->title); ?>">
                                                    <?php else: ?>
                                                        <img class="img-responsive"
                                                             src="<?php echo e(SM::sm_get_the_src(SM::sm_get_site_logo(), 112,112)); ?>"
                                                             alt="<?php echo e($is_featured->title); ?>">
                                                    <?php endif; ?>
                                                        <p class="title-color"><?php echo e($is_featured->title); ?></p>

                                                </a>
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>