<div class="header-mini">
    <div class="container">
        <div class="row">
            <div class="col-md-6 navbar-top-left">
                <a class=""><i class="fa fa-phone"></i> <?php echo e(SM::get_setting_value('mobile')); ?></a> &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="mailto:sales@brandbychoice.com">
                    <i class="fa fa-envelope"> </i> <?php echo e(SM::get_setting_value('email')); ?>

                </a>
            </div>
            <div class="col-md-6" id="navbar_collapse_0">
                 <ul class="navbar-top-right">
                <?php if(Auth::check()): ?>
                    <li class="nav-item">
                        <div class="nav-link">
                        <span class="p-pic"><img src="<?php echo SM::sm_get_the_src(Auth::user()->image,30,30); ?>" alt="image">
                        </span>Welcome&nbsp;<?php echo e(Auth::user()->username); ?>

                        </div>

                    </li>
                    <li class="nav-item"><a href="<?php echo e(url('/dashboard')); ?>" class="nav-link -before"><i
                                    class="fa fa-home"> </i> Dashboard</a></li>
                    <li class="nav-item"><a href="<?php echo e(url('/logout')); ?>" class="nav-link -before"><i
                                    class="fa fa-power-off"> </i> Logout</a></li>
                <?php else: ?>
                    <li class="nav-link nav-welcome">Welcome Guest!</li>
                    <li class="nav-login-reg">
                        <a data-toggle="modal" data-target="#onlyloginModal" class="btn-login-style" id="loginButton" xibkh="1">
                            <i class="fa fa-sign-in" ></i> Sign in
                        </a>
                    </li>
                    <li class="nav-login-reg">
                        <a data-toggle="modal" data-target="#onlyRegisterModal" class="btn-login-style" id="quickReg">
                           <i class="fa fa-registered"></i> Register
                        </a>
                    </li>
                <?php endif; ?>
            </ul>
            </div>
       </div>
    </div>
</div>
<input type="hidden" name="_token" id="table_csrf_token" value="<?php echo csrf_token(); ?>">

<header id="header-area" class="header-area">
    <div class="container">

            <div class="row mtb-15">
                <div class="col-sx-6 col-sm-4 col-md-3 animatable bounceIn">

                    <a href="<?php echo e(URL('/')); ?>" class="logo">
                        <img class="logo-style" alt="<?php echo e(SM::get_setting_value('site_name')); ?>"
                             src="<?php echo e(SM::sm_get_the_src(SM::sm_get_site_logo(), 294, 90)); ?>"/>
                    </a>

                </div>

                <div class="col-sx-12 col-sm-4 col-md-6">
                    <div class="form-group mb-0">
                        <div class="input-group">
                        <?php
                        $header_marquee_enable = SM::smGetThemeOption('header_marquee_enable');
                        $header_marquee_title = SM::smGetThemeOption('header_marquee_title');
                        $more_then_discount_amount = \App\Model\Common\Coupon::Published()->where('discount_type', 3)->where('validity', '>=', \Carbon\Carbon::now()->toDateString())->first();
                        ?>
                        <input class="form-control my-0 py-1 amber-border input-lg" id="myInput" type="text"
                               placeholder="Search product" aria-label="Search">
                        <div class="input-group-addon  btn btn-success">
                            <span class="input-group-text" id="basic-text1">
                                <i class="fa fa-search text-grey" aria-hidden="true"></i></span>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="col-sx-6 col-sm-4 col-md-3 cart-list-icon">
                    <ul class="top-right-list">
                        <li>
                            <a class="btn btn-success checkout_style" href="<?php echo e(URL('cart')); ?>" id="check_out">
                                Checkout
                            </a>
                        </li>
                        <li class="cart-header">
                            <a id="ShowDivButton " class="cart-header cart_icon showButton">
                                <span class="badge badge-secondary cart-header "> <?php echo e(Cart::instance('cart')->count()); ?></span>
                                <img src="<?php echo e(asset('/frontend')); ?>/images/cart.png">
                                <span id="ViewBasketLink" class="ViewBasketLink"></span>
                            </a>
                        </li>

                    </ul>

                </div>
            </div>
        </div>


    <?php $category = SM::getProductCategories(); ?>
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-collapse collapse">
                    <?php
                    $menu = array(
                        'nav_wrapper' => 'ul',
                        'start_class' => 'nav navbar-nav',
                        'link_wrapper' => 'li',
                        'dropdown_class' => 'dropdown open',
                        'home_class' => 'nav-item',
                        'a_class' => 'nav-link dropdown-toggle',
                        'subNavUlClass' => 'dropdown-menu',
                        'has_dropdown_wrapper_class' => 'dropdown open',
                        'show' => TRUE
                    );
                    SM::sm_get_menu($menu);
                    ?>
                </div>
            </div>
        </div>
    </nav>

</header>