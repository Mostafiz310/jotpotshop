<?php $__env->startSection("title", "Dashboard"); ?>
<?php $__env->startSection("content"); ?>
    <section class="common-section bg-black">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <?php echo $__env->make("customer.left-sidebar", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <div class="col-sm-9">
                    <div class="account-panel">
                        <h2>Dashboard</h2>
                        <div class="account-panel-inner">
							<?php
							$firstname = Auth::user()->firstname;
							$lastname = Auth::user()->lastname;
							$completeOrder = SM::sm_get_count( 'orders', "user_id", Auth::user()->id, "=", "order_status", 1 );
							$progressOrder = SM::sm_get_count( 'orders', "user_id", Auth::user()->id, "=", "order_status", 2 );
							$pendingOrder = SM::sm_get_count( 'orders', "user_id", Auth::user()->id, "=", "order_status", 3 );
							$cancelledOrder = SM::sm_get_count( 'orders', "user_id", Auth::user()->id, "=", "order_status", 4 );
							?>
                            <h3>
                                Hello <?php echo e($firstname !='' || $lastname !='' ? $firstname." ".$lastname : Auth::user()->username); ?></h3>
                            <p>From your My Account Dashboard you have the ability to view a snapshot of your recent
                                tops count activity and update your account information. Select a link below.</p>
                            <div class="order-status">
                                <a class="order-complete" href="<?php echo url("dashboard/orders/status/1"); ?>">
                                    <i class="fa fa-check-square-o"></i>
                                    <p>Order Complete (<?php echo e($completeOrder); ?>)</p>
                                </a>
                                <a class="order-pending" href="<?php echo url("dashboard/orders/status/2"); ?>">
                                    <i class="fa fa-spinner"></i>
                                    <p>Order Progress (<?php echo e($progressOrder); ?>)</p>
                                </a>
                                <a class="order-pending" href="<?php echo url("dashboard/orders/status/3"); ?>">
                                    <i class="fa fa-refresh"></i>
                                    <p>Order Pending (<?php echo e($pendingOrder); ?>)</p>
                                </a>
                                <a class="order-cancel" href="<?php echo url("dashboard/orders/status/4"); ?>">
                                    <i class="fa fa-times"></i>
                                    <p>Order Cancel (<?php echo e($cancelledOrder); ?>)</p>
                                </a>
                            </div>
							<?php
							$name = Auth::user()->firstname . " " . Auth::user()->lastname;
							?>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="acc-inner-panel">
                                        <h4>Contact information</h4>
                                        <div class="panel-contents">
                                            <?php if(empty(!$name)): ?><p><?php echo e($name); ?></p><?php endif; ?>
                                            <?php if(empty(!Auth::user()->username)): ?>
                                                <p>
                                                    Username: <?php echo e(Auth::user()->username); ?>

                                                </p>
                                            <?php endif; ?>
                                            <?php if(empty(!Auth::user()->mobile)): ?><p>
                                                Mobile: <?php echo e(Auth::user()->mobile); ?></p><?php endif; ?>
                                            <?php if(empty(!Auth::user()->email)): ?><p>Email: <?php echo e(Auth::user()->email); ?></p><?php endif; ?>
                                        </div>
                                        <a class="edit-btn" href="<?php echo url("dashboard/edit-profile"); ?>"><i
                                                    class="fa fa-edit"></i>Edit</a>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="acc-inner-panel">
                                        <h4>Default billing address</h4>
                                        <div class="panel-contents">
                                            <?php if(empty(!Auth::user()->street)): ?><p>
                                                Street: <?php echo e(Auth::user()->street); ?></p><?php endif; ?>
                                            <?php if(empty(!Auth::user()->city)): ?><p>City: <?php echo e(Auth::user()->city); ?></p><?php endif; ?>
                                            <?php if(empty(!Auth::user()->state || Auth::user()->zip)): ?>
                                                <p>State/District: <?php echo e(Auth::user()->state." - ".Auth::user()->zip); ?></p>
                                            <?php endif; ?>
                                            <?php if(empty(!Auth::user()->country)): ?><p>
                                                Country: <?php echo e(Auth::user()->country); ?></p><?php endif; ?>
                                        </div>

                                        <a class="edit-btn" href="<?php echo url("dashboard/edit-profile"); ?>"><i
                                                    class="fa fa-edit"></i>Edit</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>