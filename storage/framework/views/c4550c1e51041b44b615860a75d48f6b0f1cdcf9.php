﻿
<section class="banner-content">
    <button class="bttn-cart showButton cart_icon_popup" id="ShowDivButton">
        <img src="<?php echo e(url('/frontend/images/cart_white.png')); ?>">
        <p><?php echo e(Cart::instance('cart')->count()); ?> Item(s)</p>
        <p><span><?php echo e(SM::currency_price_value(Cart::instance('cart')->subTotal())); ?></span></p>
    </button>

    <div id="aitcg-control-panel" style="display: none">
        <button class="bttn-close hideButton" id="ShowDivButton">
            <i class="fa fa-times"></i> Close
        </button>
        <h1 class="popup_top_total"><i class="fa fa-shopping-bag"></i> <?php echo e(Cart::instance('cart')->count()); ?> ITEMS</h1>
        <h4>Trusted Online Shopping Site</h4>
        <div class="add-product-area header_cart_html" style="max-height: 500px; overflow-y: inherit">
            <?php $items = Cart::instance('cart')->content(); ?>
            <?php $__empty_1 = true; $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div class="add-pro-liner">
                    <div class="counting">
                        <i class="fa fa-plus inc" data-row_id="<?php echo e($item->rowId); ?>" style="color: green;"></i>
                        <input type="hidden" name="qty" class="form-control input-sm qty-inc-dc" id="<?php echo e($item->rowId); ?>" value="<?php echo e($item->qty); ?>">
                        <h3 class="itemqty"><span><?php echo e($item->qty); ?></span></h3>
                        <i class="fa fa-minus dec" data-row_id="<?php echo e($item->rowId); ?>" style="color: green;"></i>
                    </div>
                    <img src="<?php echo e(SM::sm_get_the_src($item->options->image, 100, 122)); ?>" alt="<?php echo e($item->name); ?>">
                    <div class="pro-head">
                        <h3><?php echo e($item->name); ?></h3>
                        <h3 class="ammount"><?php echo e(SM::currency_price_value($item->price)); ?></h3>
                    </div>
                    <span class="pro-close removeToCart" data-product_id="<?php echo e($id); ?>" onclick="delete_cart_product($id)"><i class="fa fa-times-circle"></i></span>
                </div>
                <hr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <div class="empty_img image-emty">
                    <img class="image-emty-busket" src="<?php echo e(asset('/frontend')); ?>/images/busketempty.png">
                </div>
                <div class="text-center">
                    <span>Empty Cart</span>
                </div>
            <?php endif; ?>
        </div>

        <div class="add-btn-area">
            <h5 class="sub_total"><?php echo e(SM::currency_price_value(Cart::instance('cart')->subTotal())); ?></h5>
            <a class="btn btn-add-place btn-success" href="<?php echo e(URL('cart')); ?>">Place Order</a>
        </div>
    </div>
    <?php
    $services = SM::smGetThemeOption("services", array());
    ?>
    <?php if(count($services)>0): ?>
        <div class="container">
            <div class="row">
                    <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                        $title = isset($service["title"]) ? $service["title"] : "";
                        $description = isset($service["description"]) ? $service["description"] : "";
                        $icon = isset($service["icon"]) ? $service["icon"] : "";
                        $class = ($key !== count($services) - 1) ? "" : " last";
                        ?>
                        <div class="col-12 col-md-3 ptb-15">
                            <div class="banner-single<?php echo e($class); ?>">
                                <div class="panel panel-success">
                                    <?php if(empty(!$title)): ?>
                                        <h3 class="<?php echo e($icon); ?>"></h3>
                                        <div class="block">
                                            <h4 class="title"><?php echo e($title); ?></h4>
                                            <p><?php echo e($description); ?></p>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
        </div>
    <?php endif; ?>
</section>





