<?php $__env->startSection("title", "Orders"); ?>
<?php $__env->startSection("content"); ?>
    <section class="common-section bg-black">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <?php echo $__env->make("customer.left-sidebar", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <div class="col-sm-9">
                    <div class="account-panel">
                        <h2>Review List
                        </h2>
                        <?php if(count($reviews)>0): ?>
                            <div class="account-panel-inner">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Product</th>
                                        <th>Ratings</th>
                                        <th>Review</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $reviews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $review): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="reviewRow">
                                            <td><?php echo e($loop->index+1); ?></td>
                                            <td>
                                                <a style="color: #000;" href="<?php echo e(url('product/'.$review->product->slug)); ?>"><strong><?php echo e($review->product->title); ?></strong></a>
                                            </td>
                                            <td>
                                                <div class="product-star" style="color: #ff9900;">
                                                    <?php for($i = 0; $i < 5; ++$i): ?>
                                                        <i class="fa fa-star<?php echo e($review->rating<=$i?'-o':''); ?>"
                                                           aria-hidden="true"></i>
                                                    <?php endfor; ?>

                                                </div>
                                            </td>
                                            <td>
                                                <?php echo e($review->description); ?>

                                            </td>
                                            <td>
                                                <a data-review_id="<?php echo e($review->id); ?>"
                                                   class="removeToReview btn btn-danger"
                                                   title="Remove item" href="javascript:void(0)"><i
                                                            class="fa fa-close"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center">
                                <?php echo $reviews->links('common.pagination_orders'); ?>

                            </div>
                        <?php else: ?>
                            <div class="alert alert-warning">
                                <i class="fa fa-warning"></i> No Review Found!
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>