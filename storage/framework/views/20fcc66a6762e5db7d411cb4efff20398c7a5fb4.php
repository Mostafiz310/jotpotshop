<?php $__env->startSection('title', 'Edit Profile'); ?>
<?php $__env->startSection('content'); ?>
    <?php
    $contact_email = old("contact_email") != "" ? old("contact_email") : Auth::user()->email;
    $firstname = old("firstname") != "" ? old("firstname") : Auth::user()->firstname;
    $lastname = old("lastname") != "" ? old("lastname") : Auth::user()->lastname;
    $address = old("address") != "" ? old("address") : Auth::user()->address;
    $city = old("city") != "" ? old("city") : Auth::user()->city;
    $zip = old("zip") != "" ? old("zip") : Auth::user()->zip;
    $mobile = old("mobile") != "" ? old("mobile") : Auth::user()->mobile;
    $skype = SM::get_front_user_meta(Auth::user()->id, 'skype');

    ?>
    <section class="common-section bg-black">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <?php echo $__env->make("customer.left-sidebar", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <div class="col-sm-9">
                    <?php echo Form::open(['method'=>'post', 'url'=>'dashboard/update-password']); ?>

                    <div class="account-panel">
                        <h2>Account Information</h2>
                        <div class="account-panel-inner">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <?php echo Form::label('username', "Username"); ?>

                                            <?php echo Form::text('username', Auth::user()->username,['class'=>'form-control', 'disabled'=>'']); ?>

                                            <span class="error-notice"></span>
                                        </div>
                                        <div class="col-md-6">
                                            <?php echo Form::label('useremail',"Email"); ?>

                                            <?php echo Form::text('useremail', Auth::user()->email,['class'=>'form-control', 'disabled'=>'']); ?>

                                            <span class="error-notice"></span>
                                        </div>

                                        
                                            
                                                <div class="col-md-4">
                                                    <div class="<?php echo e($errors->has('current_password') ? ' has-error' : ''); ?>">
                                                        <?php echo Form::label('current_password', "Current Password"); ?>

                                                        <?php echo Form::password('current_password', ['class'=>'form-control', 'placeholder'=>"Current Password", 'required'=>'']); ?>

                                                        <span class="error-notice">
                                                <?php if($errors->has('current_password')): ?>
                                                                <?php echo e($errors->first("current_password")); ?>

                                                            <?php endif; ?>
                                            </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                                                        <?php echo Form::label('password', "New Password"); ?>

                                                        <?php echo Form::password('password', ['class'=>'form-control', 'placeholder'=>"Password", 'required'=>'']); ?>

                                                        <span class="error-notice">
                                                <?php if($errors->has('password')): ?>
                                                                <?php echo e($errors->first("password")); ?>

                                                            <?php endif; ?>
                                            </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="<?php echo e($errors->has('password_confirmation') ? ' has-error' : ''); ?>">
                                                        <?php echo Form::label('password_confirmation', "Confirm Password"); ?>

                                                        <?php echo Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>"Confirm Password", 'required'=>'']); ?>

                                                        <span class="error-notice">
                                                <?php if($errors->has('password_confirmation')): ?>
                                                                <?php echo e($errors->first("password_confirmation")); ?>

                                                            <?php endif; ?>
                                            </span>
                                                    </div>
                                                </div>
                                            
                                        
                                        <div class="col-sm-12 text-right">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i>
                                                Update Password
                                            </button>
                                        </div>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <?php echo e(Form::close()); ?>

                    <?php echo Form::open(['method'=>'post', 'url'=>'dashboard/save-profile']); ?>

                    <div class="account-panel">
                        <h2>Personal Information</h2>
                        <div class="account-panel-inner">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="<?php echo e($errors->has('firstname') ? ' has-error' : ''); ?>">
                                                <?php echo Form::label('firstname', "First Name"); ?>

                                                <?php echo Form::text('firstname', $firstname,['class'=>'form-control', 'placeholder'=>"First Name", 'required'=>'']); ?>

                                                <span class="error-notice">
                                                <?php if($errors->has('firstname')): ?>
                                                        <?php echo e($errors->first("firstname")); ?>

                                                    <?php endif; ?>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="<?php echo e($errors->has('lastname') ? ' has-error' : ''); ?>">
                                                <?php echo Form::label('lastname',"Last Name"); ?>

                                                <?php echo Form::text('lastname', $lastname,['class'=>'form-control', 'placeholder'=>"Last Name", 'required'=>'']); ?>

                                                <span class="error-notice">
                                                <?php if($errors->has('lastname')): ?>
                                                        <?php echo e($errors->first("lastname")); ?>

                                                    <?php endif; ?>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="<?php echo e($errors->has('mobile') ? ' has-error' : ''); ?>">
                                                <?php echo Form::label('mobile', "Mobile No"); ?>

                                                <?php echo Form::text('mobile', $mobile,['class'=>'form-control', 'placeholder'=>"Mobile No", 'required'=>'']); ?>

                                                <span class="error-notice">
                                                <?php if($errors->has('mobile')): ?>
                                                        <?php echo e($errors->first("mobile")); ?>

                                                    <?php endif; ?>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="<?php echo e($errors->has('skype') ? ' has-error' : ''); ?>">
                                                <?php echo Form::label('skype',"Skype ID"); ?>

                                                <?php echo Form::text('skype', $skype,['class'=>'form-control', 'placeholder'=>"Skype ID"]); ?>

                                                <span class="error-notice">
                                                <?php if($errors->has('skype')): ?>
                                                        <?php echo e($errors->first("skype")); ?>

                                                    <?php endif; ?>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                <div class="col-sm-12">
                                    <div class="<?php echo e($errors->has('address') ? ' has-error' : ''); ?>">
                                        <?php echo Form::label('address',__("Address")); ?>

                                        <?php echo Form::text('address', $address,['class'=>'form-control', 'placeholder'=>__("Address"), 'required'=>'']); ?>

                                        <span class="error-notice">
                                        <?php if($errors->has('address')): ?>
                                                <?php echo e($errors->first("address")); ?>

                                            <?php endif; ?>
                                    </span>
                                    </div>
                                </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="<?php echo e($errors->has('city') ? ' has-error' : ''); ?>">
                                                <?php echo Form::label('city',__("user.city")); ?>

                                                <?php echo Form::text('city', $city,['class'=>'form-control', 'placeholder'=>__("user.city"), 'required'=>'']); ?>

                                                <span class="error-notice">
                                                <?php if($errors->has('city')): ?>
                                                        <?php echo e($errors->first("city")); ?>

                                                    <?php endif; ?>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="<?php echo e($errors->has('zip') ? ' has-error' : ''); ?>">
                                                <?php echo Form::label('zip',__("user.zip")); ?>

                                                <?php echo Form::number('zip', $zip,['class'=>'form-control', 'placeholder'=>__("user.zip"), 'required'=>'']); ?>

                                                <span class="error-notice">
                                                <?php if($errors->has('zip')): ?>
                                                        <?php echo e($errors->first("zip")); ?>

                                                    <?php endif; ?>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group <?php echo e($errors->has('country') ? ' has-error' : ''); ?>">
                                                <?php echo Form::label('country', __("user.country")); ?>

                                                <select name="country" id="country"
                                                        class="form-control country p_complete"
                                                        data-state="state"
                                                        required=""
                                                        data-onload="<?php echo isset($country) ? $country : "" ?>">
                                                    <option value="">Select Your Country</option>
                                                    <?php
                                                    $countries = SM::$countries;
                                                    $i = 1;
                                                    foreach ($countries as $country_name)
                                                    {
                                                    //                                 if (in_array($i, array(17, 18, 19, 20)))
                                                    //                                 {
                                                    ?>
                                                    <option value="<?php echo $country_name; ?>"
                                                            data-id="<?php echo $i; ?>"><?php echo $country_name; ?></option>
                                                    <?php
                                                    //                                 }
                                                    $i++;
                                                    }
                                                    ?>
                                                </select>
                                                <span class="error-notice">
                                                <?php if($errors->has('street')): ?>
                                                        <?php echo e($errors->first("street")); ?>

                                                    <?php endif; ?>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group <?php echo e($errors->has('state') ? ' has-error' : ''); ?>">
                                                <?php echo Form::label('state', __("user.state")); ?>

                                                <select required="" name="state" id="state"
                                                        class="form-control state p_complete"
                                                        required=""
                                                        data-onload="<?php echo isset($state) ? $state : ""; ?>">
                                                    <option value="#">Select State / Province</option>
                                                </select>
                                                <span class="error-notice">
                                                <?php if($errors->has('state')): ?>
                                                        <?php echo e($errors->first("state")); ?>

                                                    <?php endif; ?>
                                            </span>
                                            </div>
                                        </div>
                                        <?php
                                        if(Auth::check()){
                                        $country = old("country") != "" ? old("country") : Auth::user()->country;
                                        $state = old("state") != "" ? old("state") : Auth::user()->state;
                                        ?>
                                        <script>
                                            $("#country").val('<?php echo $country; ?>');
                                                <?php if($country != ''): ?>
                                            var selectedCountryIndex = $("#country").find('option:selected').attr('data-id');
                                            var state = $("#country").attr('data-state');
                                            change_state(selectedCountryIndex, state);
                                            <?php endif; ?>
                                            $("#state").val('<?php echo $state; ?>');
                                        </script>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                <div class="col-sm-12 text-right">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save
                                        Profile
                                    </button>
                                </div>
                                </div>
                                <div class="clearfix"></div>

                            </div>
                        </div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </section>


<?php $__env->stopSection(); ?>



<?php echo $__env->make('frontend.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>