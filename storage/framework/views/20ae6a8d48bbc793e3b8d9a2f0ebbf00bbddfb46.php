<div class="payment-area">
    <div class="heading">
        <h4>Payment Methods</h4>
        <hr>
    </div>
    <div class="payment-methods">
        

        <div class="alert alert-danger error_payment" style="display:none" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            Please select your payment method')
        </div>
        <ul class="list">
            <?php $__currentLoopData = $payment_methods; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment_method): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li>
                    <label for="pm_<?php echo e($payment_method->id); ?>">
                        <input style="display: none;" checked required type="radio" id="pm_<?php echo e($payment_method->id); ?>"
                               name="payment_method_id" class="payment_method"
                               value="<?php echo e($payment_method->id); ?>">
                        <strong><?php echo e($payment_method->title); ?></strong>
                    </label>
                </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>

    <div class="submitButton">
        <button class="btn btn-success active">Order Now</button>
    </div>
</div>