<?php if(count($relatedProduct) > 0): ?>
    <section class="site-content">
        <div class="container">
            <div class="products-area">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-spacial p-0">
                        <div class="paanel custom-panel-style">
                            <div class="panel-heading ">
                                <h3 class="section-heading">Similar products</h3>
                            </div>
                            <div class="panel-body plr-0">
                                <div id="special" role="tabpanel" aria-labelledby="special-tab">
                                    <div class="owl-carousel owl-loaded owl-drag">
                                        <div class="owl-stage-outer">
                                            <div class="owl-stage">
                                                <?php $__currentLoopData = $relatedProduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$rProductSingle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="owl-item active">
                                                        <div class="product-grid mtb-15">
                                                            <div class="product-image">
                                                                <a href="<?php echo e(url('/product/'.$rProductSingle->slug)); ?>" class="image">
                                                                    <img class="pic-1" src="<?php echo SM::sm_get_the_src($rProductSingle->image, 200,200); ?>">
                                                                </a>
                                                                
                                                                <span class="product-discount-label"><?php echo e(SM::productDiscount($rProductSingle->id )); ?> %</span>
                                                                
                                                                
                                                                
                                                                
                                                            </div>
                                                            <div class="product-content">
                                                                <a href="<?php echo e(url('/product/'.$rProductSingle->slug)); ?>" class="title-color mb-1"><?php echo e($rProductSingle->title); ?></a>
                                                                <?php if($rProductSingle->sale_price > 0): ?>
                                                                    <div class="price"><span><?php echo e(SM::currency_price_value($rProductSingle->regular_price)); ?> </span> <?php echo e(SM::currency_price_value($rProductSingle->sale_price)); ?> </div>
                                                                <?php else: ?>
                                                                    <div class="price"> <?php echo e(SM::currency_price_value($rProductSingle->regular_price)); ?> </div>
                                                                <?php endif; ?>
                                                                <div class="product-link inc_dec_qty">
                                                                    <a href="#" class=""> <?php echo e($rProductSingle->product_weight); ?> <?php echo e(SM::product_wait_unit($rProductSingle->unit_id)->title); ?></a>
                                                                    <?php  echo SM::addToCartButton($rProductSingle->id, $rProductSingle->regular_price, $rProductSingle->sale_price) ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>