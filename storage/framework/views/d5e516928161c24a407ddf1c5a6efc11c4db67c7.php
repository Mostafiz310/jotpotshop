<?php $__env->startSection("title", $categoryInfo->title); ?>
<?php $__env->startSection('content'); ?>
    <?php $__env->startPush('style'); ?>
        <style>
            #loading {
                text-align: center;
                background: url('loader.gif') no-repeat center;
                height: 150px;
            }
        </style>
    <?php $__env->stopPush(); ?>
    <!-- page wapper-->
    <div class="columns-container">
        <div class="container" id="columns">
            <!-- breadcrumb -->
        <?php echo $__env->make('frontend.common.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- ./breadcrumb -->
            <!-- row -->
            <div class="row">
                <!-- Left colunm -->
            <?php echo $__env->make('frontend.products.product_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- ./left colunm -->
                <!-- Center colunm-->
                <div class="center_column col-xs-12 col-md-10 col-lg-10 col-sm-10" id="center_column">
                    <!-- view-product-list-->
                    <div class="short-list-style">
                        <div class="row">
                            <div class="form-group col-xs-12 col-md-9 mb-0">
                                <h5 class="title-style">
                                    <i class="fa fa-shopping-cart"></i> <span id="category_filter_data"><strong><?php echo e($categoryInfo->title); ?></strong></span>
                                    <span id="brand_filter_data"></span>
                                </h5>
                            </div>
                            <div class="form-group col-xs-12 col-md-3 mb-0">
                                <div class="input-group mb-0">
                                    <label class="input-group-addon">Sort By</label>
                                    <select data-role="sorter" class="form-control sortby onChangeProductFilter orderByPrice">
                                        <option value="">Popularity</option>
                                        <option value="1">Product Name</option>
                                        <option value="2">New</option>
                                        <option value="3">Best Sellers</option>
                                        <option value="4">Most Viewed</option>
                                        <option value="5">Price low to high</option>
                                        <option value="6">Price high to low</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if(!empty($categoryInfo->image_gallery)): ?>
                        <div class="box-product-content">
                            <ul class="product-list owl-carousel " data-dots="false" data-loop="true"
                                data-nav="true"
                                data-margin="30"
                                data-responsive='{"0":{"items":1},"500":{"items":1},"600":{"items":1},"1000":{"items":1}}'>
                                <?php
                                $myString = $categoryInfo->image_gallery;
                                $myArray = explode(',', $myString);
                                ?>
                                <?php $__currentLoopData = $myArray; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v_data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>
                                        <img class="img-responsive carousel-img-height-col4" alt="<?php echo e($categoryInfo->title); ?>" src="<?php echo e(SM::sm_get_the_src($v_data, 970, 300)); ?>">
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                <?php else: ?>
                    
                    
                    
                <?php endif; ?>
                    <!-- ./PRODUCT LIST -->
                    <div class="row" id="ajax_view_product_list">
                    </div>
                </div>
                <!-- ./view-product-list-->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>