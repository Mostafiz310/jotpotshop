<?php $__env->startSection('title', 'Cart'); ?>
<?php $__env->startSection('content'); ?>
    <div class="columns-container">
        <div class="container-fluid" id="columns">
            <!-- breadcrumb -->
        <?php echo $__env->make('frontend.common.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- ./breadcrumb -->
            <!-- page heading-->
            <h2 class="page-heading no-line" style="display: none;">
                <span class="page-heading-title2">Shopping Cart Summary</span>
            </h2>
            <!-- ../page heading-->
            <div class="page-content page-order">
                <div class="heading-counter warning">Your shopping cart contains:
                    <span><?php echo e(count($cart)); ?> Product</span>
                </div>
                <div class="order-detail-content ">
                    <table class="table table-bordered table-responsive cart_summary cart_table">
                        <thead>
                        <tr>
                            <th class="cart_product">Product</th>
                            <th>Description</th>
                            <th>Unit price</th>
                            <th>Qty</th>
                            <th>Total</th>
                            <th class="action"><i class="fa fa-trash-o"></i></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__empty_1 = true; $__currentLoopData = $cart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>

                            <tr id="tr_<?php echo e($item->rowId); ?>" class="removeCartTrLi">
                                <td class="cart_product">
                                    <a href="<?php echo e(url('product/'.$item->options->slug)); ?>">
                                        <img src="<?php echo e(SM::sm_get_the_src($item->options->image, 100, 122)); ?>"
                                            class="card_image" alt="<?php echo e($item->name); ?>"></a>
                                </td>
                                <td class="cart_description">
                                    <p class="product-name">
                                        <a style="color: #630767;"
                                           href="<?php echo e(url('product/'.$item->options->slug)); ?>"><?php echo e($item->name); ?>

                                        </a>
                                    </p>
                                    <span class="cart_ref">SKU : <?php echo e($item->options->sku); ?></span>
                                    <?php if($item->options->colorname != ''): ?>
                                        <br>
                                        <span>Color : <?php echo e($item->options->colorname); ?></span>
                                    <?php endif; ?>
                                    <?php if($item->options->sizename != ''): ?>
                                        <br>
                                        <span>Size : <?php echo e($item->options->sizename); ?></span>
                                    <?php endif; ?>
                                </td>
                                <td class="price"><span><?php echo e(SM::currency_price_value($item->price)); ?> </span></td>
                                <td class="qty" width="15%">
                                    <div class="form-group cart-qty">
                                        <div class="input-group">
                                            <span class="input-group-addon inc btn btn-success" data-row_id="<?php echo e($item->rowId); ?>" id=""><i
                                                        class="fa fa-plus" aria-hidden="true"></i></span>
                                            <input type="text" name="qty" class="form-control text-center input-sm qty-inc-dc"
                                                   id="<?php echo e($item->rowId); ?>" value="<?php echo e($item->qty); ?>">
                                            <span id="" class="input-group-addon dec btn btn-success" data-row_id="<?php echo e($item->rowId); ?>"><i
                                                        class="fa fa-minus" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                </td>
                                <td class="price">
                                    <span><?php echo e(SM::currency_price_value($item->price * $item->qty)); ?> </span>
                                </td>
                                <td class="action">
                                    <a data-product_id="<?php echo e($item->rowId); ?>"
                                       class="btn btn-danger remove_link removeToCart"
                                       title="Delete item"
                                       href="javascript:void(0)"><i class="fa fa-trash-o"></i></a>

                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                            <tr>
                                <td colspan="7" class="text-center">
                                    <p class="product-name" style="color: red">No data found!</p>
                                </td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="2" rowspan="3"></td>
                            <td colspan="3">Sub Total</td>
                            <td colspan="2"><?php echo e(SM::currency_price_value(Cart::instance('cart')->subTotal())); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3"><strong>Total</strong></td>
                            <td colspan="2">
                                <strong><?php echo e(SM::currency_price_value(Cart::instance('cart')->total())); ?></strong></td>
                        </tr>

                        </tfoot>
                    </table>
                    <div class="pull-right">
                        <?php if(Auth::check()): ?>
                            <a class="btn btn-success active next-btn" href="<?php echo e(url('/checkout')); ?>">Proceed to
                                checkout</a>
                        <?php else: ?>
                            <a class="btn btn-success active next-btn" data-toggle="modal" data-target="#loginModal"
                               href="#">Proceed to checkout</a>
                        <?php endif; ?>
                    </div>
                    <div class="">
                        <a class="btn btn-info active" href="<?php echo e(url('/shop')); ?>">Continue shopping</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>