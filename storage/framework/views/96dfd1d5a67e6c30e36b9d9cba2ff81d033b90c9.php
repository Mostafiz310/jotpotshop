<?php
$product_special_is_enable = SM::smGetThemeOption("product_special_is_enable", 1);
$product_show_category = SM::smGetThemeOption("product_show_category", 1);
$product_show_tag = SM::smGetThemeOption("product_show_tag", 1);
$product_show_brand = SM::smGetThemeOption("product_show_brand", 1);
$product_show_size = SM::smGetThemeOption("product_show_size", 1);
$product_show_color = SM::smGetThemeOption("product_show_color", 1);
$product_show_testimonial = SM::smGetThemeOption("product_show_testimonial", 1);
$product_show_advertisement = SM::smGetThemeOption("product_show_advertisement", 1);
?>

<div class="col-12 col-lg-2 col-md-2 col-sm-3 spaceright-0 pr-0">
    <div id="accordion" class="filters" role="tablist">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h4 class="title collapsed" href="#collapseAccordian1" data-toggle="collapse" aria-expanded="true"
                    aria-controls="collapseAccordian1">Categories </h4>
            </div>
            <div class="block collapse show" id="collapseAccordian1" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                <nav id="categories" class="navbar navbar-expand-lg p-0 categories">
                    <?php if($product_show_category==1): ?>
                        <?php
                        $getProductCategories = SM::getProductCategories(0);
                        ?>
                        <?php if(count($getProductCategories)>0): ?>
                            <div class="layered-content">
                                <ul class="check-box-list">
                                    <?php $__currentLoopData = $getProductCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                        $segment = Request::segment(2);
                                        if ($segment == $cat->slug) {
                                            $selected = 'checked';
                                        } else {
                                            $selected = '';
                                        }
                                        ?>

                                        <li>
                                            <input  <?php echo e($selected); ?> type="checkbox" id="c1_<?php echo e($cat->id); ?>"
                                                    value="<?php echo e($cat->id); ?>" class="common_selector category"/>
                                            <label for="c1_<?php echo e($cat->id); ?>">
                                                <?php echo e($cat->title); ?>

                                            </label>
                                            <?php
                                            echo SM::category_tree_for_select_cat_id($cat->id, $segment);
                                            ?>

                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </nav>


            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingOne">
                <h4 class="title collapsed" href="#brandCollapseAccordian" data-toggle="collapse" aria-expanded="true"
                    aria-controls="brandCollapseAccordian">Brands  </h4>
            </div>
            <div class="block collapse show" id="brandCollapseAccordian" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="block" style="height: 300px;overflow-y: scroll">
                    <nav id="brand" class="navbar navbar-expand-lg p-0 brand">
                        <!-- filter price -->
                        <input type="hidden" name="min_price" id="min_price" value="0">
                        <input type="hidden" name="max_price" id="max_price" value="2471">
                        <input type="hidden" name="filters_applied" id="filters_applied" value="0">
                    <?php
                    $max_price = (int)\App\Model\Common\Product::max('regular_price');
                    $min_price = (int)\App\Model\Common\Product::min('regular_price');
                    ?>
                    <!--                <div class="layered_subtitle card-title">Price</div>
                <div class="layered-content slider-range">-->
                    <!--                    <div data-label-reasult="Range:" data-min="<?php echo $min_price ?>"
                         data-max="<?php echo $max_price ?>"
                         data-unit="<?php echo e(SM::get_setting_value('currency')); ?>"
                         class="slider-range-price" data-value-min="<?php echo $min_price ?>"
                         data-value-max="<?php echo $max_price ?>">
                    </div>
                    <input type="hidden" id="hidden_minimum_price" value="<?php echo $min_price ?>"/>
                    <input type="hidden" id="hidden_maximum_price" value="<?php echo $max_price ?>"/>
                    <div class="amount-range-price">Range: <?php echo e(SM::product_price($min_price)); ?>

                    <?php echo e(SM::product_price($max_price)); ?>

                            </div>-->

                    <!--
            <section class="range-slider" id="facet-price-range-slider">
                <input name="range-1" id="hidden_minimum_price" value="0" min="<?php echo $min_price ?>" max="<?php echo $max_price ?>" step="1" type="range">
                <input name="range-2" id="hidden_maximum_price" value="0" min="<?php echo $min_price ?>" max="<?php echo $max_price ?>" step="1" type="range">

            </section>-->

                        <!--</div>-->
                        <!-- ./filter price -->
                        <?php if($product_show_color==1): ?>
                            <?php
                            $getProductColors = SM::getProductColors(0);
                            ?>
                            <?php if(count($getProductColors)>0): ?>
                                <div class="layered_subtitle card-title">Color</div>
                                <div class="layered-content filter-color">

                                    <ul class="check-box-list">
                                        <?php $__currentLoopData = $getProductColors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                            <li>
                                                <input type="checkbox" id="color_<?php echo e($color->id); ?>" value="<?php echo e($color->id); ?>"
                                                       class="common_selector color"/>
                                                <label style="" for="color_<?php echo e($color->id); ?>"><?php echo e($color->title); ?></label>
                                            </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    <!-- ./filter color -->
                        <!-- ./filter brand -->
                        <?php if($product_show_brand==1): ?>
                            <?php
                            $getProductBrands = SM::getProductBrands(0);
                            ?>
                            <?php if(count($getProductBrands)>0): ?>
                                <div class="layered-content filter-brand">
                                    <ul class="check-box-list">
                                        <?php $__currentLoopData = $getProductBrands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php
                                            $segment2 = Request::segment(2);
                                            if ($segment2 == $brand->slug) {
                                                $selected2 = 'checked';
                                            } else {
                                                $selected2 = '';
                                            }
                                            ?>
                                            
                                            <li>
                                                <input <?php echo e($selected2); ?> type="checkbox" value="<?php echo e($brand->id); ?>"
                                                       id="brand_<?php echo e($brand->id); ?>"
                                                       class="common_selector brand"/>
                                                <label for="brand_<?php echo e($brand->id); ?>">
                                                    <?php echo e($brand->title); ?><span
                                                            class="count"></span>
                                                </label>
                                            </li>
                                            
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    <!-- ./filter brand -->
                        <!-- ./filter size -->
                        <?php if($product_show_size==1): ?>
                            <?php
                            $getProductSizes = SM::getProductSizes(0);
                            ?>
                            <?php if(count($getProductSizes)>0): ?>
                                <div class="layered_subtitle card-title">Size</div>
                                <div class="layered-content filter-size">
                                    <ul class="check-box-list">
                                        <?php $__currentLoopData = $getProductSizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li>
                                                <input type="checkbox" id="size_<?php echo e($size->id); ?>" value="<?php echo e($size->id); ?>"
                                                       class="common_selector size"/>
                                                <label for="size_<?php echo e($size->id); ?>"><?php echo e($size->title); ?></label>
                                            </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

